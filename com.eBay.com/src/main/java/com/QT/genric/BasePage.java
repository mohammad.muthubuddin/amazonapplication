package com.QT.genric;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class BasePage 
{

	public WebDriver driver = null;
	
	static final Logger logger = LogManager.getLogger(BasePage.class.getName());

	public BasePage(WebDriver driver)
	{
		this.driver = driver;
	}

	public void verifyTitle(String exp_title)
	{
		WebDriverWait w = new WebDriverWait(driver, 10);
		String act_title = null;
		try
		{
			w.until(ExpectedConditions.titleIs(exp_title));
			act_title = driver.getTitle();
			Assert.assertEquals(act_title, exp_title);
			logger.info(act_title +" and "+exp_title +"are matched");


		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Title mismatch");
		}

	}
	public void verifyElementPresent(WebElement ele)
	{
		WebDriverWait w = new WebDriverWait(driver, 10);
		try
		{
			w.until(ExpectedConditions.visibilityOf(ele));
			logger.info(ele +"found");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(ele +"not found");
		}
	}
	public void mouseHover(WebDriver driver, WebElement ele)
	{
		WebDriverWait w = new WebDriverWait(driver, 10);
		try
		{
			Actions action = new Actions(driver);
			action.moveToElement(ele).perform();
			logger.info("Mousehovered on "+ele);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Unable to mouseover on "+ele);
		}

	}
	


}