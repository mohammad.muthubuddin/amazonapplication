package pageObjectsModels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.QT.genric.BasePage;

public class HomePage extends BasePage{
	//Declaration
		@FindBy(xpath ="//span[contains(text(),'Hello. Sign in')]")
		private WebElement signInBtn;
		
		@FindBy(id="'nav-cart-count")
		private WebElement cartBtn;
						
		@FindBy(id="twotabsearchtextbox")
		private WebElement searchBar;
		
		@FindBy(xpath="//span[contains(text(),'Your Account')]")
		private WebElement account;
		
		
		//Initialization
		public HomePage(WebDriver driver)
		{
			super(driver);
			PageFactory.initElements(driver, this);
		}
		
		
		//utilization
		public void clickOnSignInButton()
		{
			verifyElementPresent(signInBtn);
			signInBtn.click();
		}
		
		public void clickOnCartButton()
		{
			verifyElementPresent(cartBtn);
			cartBtn.click();
		}
		
		
		
		public void setSearchValue(String val)
		{
			verifyElementPresent(searchBar);
			searchBar.sendKeys(val);
		}
		
		
		
		
		public void goToAccount()
		{
			verifyElementPresent(account);
			mouseHover(driver, account);
		}
		
		

}
