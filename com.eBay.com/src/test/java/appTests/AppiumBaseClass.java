package appTests;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;

public class AppiumBaseClass {
	AppiumDriver<MobileElement> driver;
	
	public AppiumBaseClass() {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

	@BeforeTest
	public void setUp() {
		 try {
		DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Oppof9");
        caps.setCapability(MobileCapabilityType.UDID, "DU95WS4PF67SQGW8");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
        // caps.setCapability(MobileCapabilityType.APP, "");
       // caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
        caps.setCapability("appPackage", "io.appium.android.apis");
        caps.setCapability("appActivity", "io.appium.android.apis.ApiDemos");
       URL url= new URL("http://127.0.0.1:4723/wd/hub");
       driver = new AppiumDriver<MobileElement>(url,caps);
//       driver = new AndroidDriver<MobileElement>(url,caps);
		} 
		 catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void SampleTest() {
		System.out.println("I am inside the test");
		
	}
	 @AfterTest
	public void teardown() {
        driver.close();
        driver.quit();
    }
}
